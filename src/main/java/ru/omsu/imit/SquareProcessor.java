package ru.omsu.imit;

public class SquareProcessor {
    private SquareSolve sTriple;
    public SquareProcessor (SquareSolve sTriple){
        this.sTriple = new SquareSolve(sTriple);
    }
    public double getGreatestRoot() throws IllegalArgumentException{
        double[] rootArray = sTriple.rootSearch();
        if(rootArray.length == 0){
            throw new IllegalArgumentException("No roots");
        } else {
            if (rootArray.length == 1) {
                return rootArray[0];
            } else {
                return Math.max(rootArray[0],rootArray[1]);
            }
        }
    }

}
