package ru.omsu.imit;

import org.junit.Test;

import static org.junit.Assert.*;

public class SquareProcessorTest {

    @Test
    public void getGreatestRootLessZero() throws IllegalArgumentException{
        assertEquals(-1,new SquareProcessor(new SquareSolve(1,4,3)).getGreatestRoot(),0.0001);
    }
    @Test
    public void oneRootIsGreatest() throws IllegalArgumentException{
        assertEquals(1,new SquareProcessor(new SquareSolve(1,-2,1)).getGreatestRoot(),0.0001);
    }
    @Test
    public void greatestRoot() throws IllegalArgumentException{
        assertEquals(3,new SquareProcessor(new SquareSolve(1,-5,6)).getGreatestRoot(),0.0001);
    }
    @Test
    public void getGreatestDoubleRoot() throws IllegalArgumentException{
        assertEquals(1.5,new SquareProcessor(new SquareSolve(1,-2.5,1.5)).getGreatestRoot(),0.0001);
    }
    @Test(expected = IllegalArgumentException.class)
    public void getGreatestRootFail()throws IllegalArgumentException{
        assertEquals(1.5,new SquareProcessor(new SquareSolve(1,1,3)).getGreatestRoot(),0.0001);
        fail();
    }
}